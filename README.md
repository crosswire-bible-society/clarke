# Clarke

Adam Clarke (1760 or 1762-1832) was a British Methodist theologian and Biblical scholar. He is primarily remembered for writing this commentary on the Bible.